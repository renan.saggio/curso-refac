from django.db import models
from Payments.constants.payment_status import PaymentConfiguration

class Payment(models.Model):
    description = models.CharField(max_length=200)
    lastUpdate = models.DateField()
    value = models.FloatField()
    status = models.CharField(
        max_length=20,
        choices=PaymentConfiguration.PAYMENT_STATE_CHOICES,
        default=PaymentConfiguration.DEFAULT_PAYMENT_OPTION,
    )



    def __str__(self):
        return self.description