from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from Payments.models import Payment
from django.db.models import Sum
from Payments.filters.filter_service import FilterService

def payment_list(request, template_name='Payments/payment_list.html'):
    payments = Payment.objects.all()
    data = {}
    data['object_list'] = payments
    return render(request, template_name, data)

def filter_list(request, template_name='Payments/payment_list.html'):
    
    payments = Payment.objects.all()
    payments = FilterService().filter(payments, request)
    total = payments.aggregate(Sum('value'))
    
    data = {}
    data['object_list'] = payments
    data['total'] = total['value__sum'] if total['value__sum'] != None else 0

    return render(request, template_name, data)