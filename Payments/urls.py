from django.urls import path

from . import views

urlpatterns = [
    path('', views.payment_list, name='payment_list'),
    path('filter', views.filter_list, name='payment_filter'),
]