from Payments.filters.filter_parameters import DefaultFilters

class FilterService:

    def filter(self, payments, request):

        filteredPayments = payments

        for currentFilter in DefaultFilters.filters:
            filteredPayments = currentFilter.filter(filteredPayments, request)

        return filteredPayments