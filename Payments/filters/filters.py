from Payments.filters.abstract_filter import AbstractFilter

class StatusFilter(AbstractFilter):
    
    def condition(self, request):
        return request.POST["status"] != ""

    def operation(self, payments, request):
        return payments.filter(status=request.POST["status"])


class MinValueFilter(AbstractFilter):
    
    def condition(self, request):
        return request.POST["valor_minimo"] != '0'

    def operation(self, payments, request):
        return payments.filter(value__gte=request.POST["valor_minimo"])

class MaxValueFilter(AbstractFilter):
    
    def condition(self, request):
        return request.POST["valor_maximo"] != '0'

    def operation(self, payments, request):
        return payments.filter(value__lte=request.POST["valor_maximo"])

class MinDateFilter(AbstractFilter):
    
    def condition(self, request):
        return request.POST["data_inicio"] != ''

    def operation(self, payments, request):
        return payments.filter(lastUpdate__gte=request.POST["data_inicio"])

class MaxDateFilter(AbstractFilter):
    
    def condition(self, request):
        return request.POST["data_fim"] != ''

    def operation(self, payments, request):
        return payments.filter(lastUpdate__lte=request.POST["data_fim"])