from Payments.filters.filters import StatusFilter, MinDateFilter, MinValueFilter,MaxDateFilter,MaxValueFilter

class DefaultFilters:

    filters = [
        StatusFilter(),
        MinDateFilter(),
        MinValueFilter(),
        MaxValueFilter(),
        MaxDateFilter()
    ]