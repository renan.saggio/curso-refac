class PaymentConfiguration:

    DEFAULT_PAYMENT_OPTION = 'pendente'

    PAYMENT_STATE_CHOICES = [
        ('pendente', 'pendente'),
        ('pago', 'pago'),
        ('vencido', 'vencido'),
    ]